# README #
**Update:** We will be using the feature branch development strategy. You can read more about it here. https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow.
Essentially, nobody can push changes to the master branch. You should make changes on what's called a 'feature branch'. Multiple people can coordinate on a feature branch, pushing that branch to the server as they see fit. When your 'feature' is ready to be included in the master branch, create a pull request and the group can review it, comment, rework and approve it. Then it can be included in the master by approving the pull request.

This README (will eventually) contain(s) steps to get up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
* Bugs / Features can be proposed on the [Issues][1] Page.

![Screen Shot](https://bitbucket.org/repo/kGkdoX/images/4241606011-device-2014-10-12-220927.png)

[1]: https://bitbucket.org/SeptimusX75/poker-face/issues "New Issues"