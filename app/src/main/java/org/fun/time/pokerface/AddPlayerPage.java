package org.fun.time.pokerface;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Modified by ActiveNerd on 11/20/2014
 */
public class AddPlayerPage extends Activity {

    private static final String PLAYER_LIST = "org.fun.time.pokerface.AddPlayerPage.PLAYER_LIST";
    public static final String NAME_EXTRA = "org.fun.time.pokerface.AddPlayerPage.NAME";
    private ListView listView;
    private PlayerListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_players_page);

        listView = (ListView) findViewById(R.id.list);

        ArrayList<Player> players = null;
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(PLAYER_LIST)) {
                players = savedInstanceState.getParcelableArrayList(PLAYER_LIST);
            }
        }

        PlayerSwipeTouchListener touchListener = new PlayerSwipeTouchListener(this, listView);
        listView.setOnTouchListener(touchListener);
        listView.setOnScrollListener(touchListener.makeScrollListener());
        if (players == null) {
            players = new ArrayList<Player>();
            players.add(createPlayer("Player 1"));
            players.add(createPlayer("Player 2"));
        }

        adapter = new PlayerListAdapter(this, R.layout.player_row_layout, players);
        listView.setAdapter(adapter);
    }

    private Player createPlayer(String playerName) {
        Player p = new Player(playerName, 0, 0);
        p.setAvatar(BitmapFactory.decodeResource(getResources(), R.drawable.poker_face_icon));
        return p;
    }
    private void addPlayer () {
        adapter.add(createPlayer("New Player " + adapter.getCount()));
        adapter.notifyDataSetChanged();

    }


    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_player_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.add_player) {
            addPlayer();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class PlayerSwipeTouchListener extends OnSwipeTouchListener {
        public PlayerSwipeTouchListener(Context ctx, ListView view) {
            super(ctx, view);
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view) {
            int position = parent.getPositionForView(view);
            String name = adapter.getItem(position).getName();

            Intent openEditPlayerPage = new Intent(AddPlayerPage.this, EditPlayerPage.class);
            openEditPlayerPage.putExtra(NAME_EXTRA, name);
            openEditPlayerPage.setAction(Intent.ACTION_SEND);
            startActivity(openEditPlayerPage);
        }

        @Override
        public void onItemSwipe(AdapterView<?> parent, final View view, boolean swipeRight) {
            int position = parent.getPositionForView(view);

            // remove item from adapter
            final Player item = adapter.getItem(position);
            final int originalHeight = view.getHeight();
            final ViewGroup.LayoutParams lp = view.getLayoutParams();
            view.setTranslationX(10000);

            ValueAnimator va = ValueAnimator.ofInt(originalHeight, 1);
            va.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    adapter.remove(item);
                    adapter.notifyDataSetChanged();
                    Toast.makeText(AddPlayerPage.this, "Player Removed!", Toast.LENGTH_SHORT).show();
                    view.setTranslationX(0);
                    lp.height = originalHeight;
                }
            });


            va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    lp.height = (Integer) animation.getAnimatedValue();
                    view.setLayoutParams(lp);
                }
            });

            va.start();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(PLAYER_LIST, adapter.getData());
    }

    public static class PlayerListAdapter extends ArrayAdapter<Player> {
        private final Activity context;
        private final ArrayList<Player> players;
        private int layout_resource;

        static class ViewHolder {
            ImageView avatar;
            TextView name;
        }

        public PlayerListAdapter(Activity context, int resource, ArrayList<Player> players) {
            super(context, resource, players);
            this.layout_resource = resource;
            this.context = context;
            this.players = players;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rowView = convertView;
            if (rowView == null) {
                LayoutInflater inflater = context.getLayoutInflater();
                rowView = inflater.inflate(layout_resource, null);
                // Add stuff to the view holder
                ViewHolder holder = new ViewHolder();
                holder.avatar = (ImageView) rowView.findViewById(R.id.player_avater);
                holder.name = (TextView) rowView.findViewById(R.id.player_name);
                rowView.setTag(holder);
            }

            ViewHolder holder = (ViewHolder) rowView.getTag();
            holder.avatar.setImageBitmap(players.get(position).getAvatar());
            holder.name.setText(players.get(position).getName());

            return rowView;
        }

        public ArrayList<Player> getData() {
            return players;
        }
    }
}
