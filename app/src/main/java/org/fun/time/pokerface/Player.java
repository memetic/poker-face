package org.fun.time.pokerface;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Julieeeee on 10/16/2014.
 */
public class Player implements Parcelable {

    private boolean isOut;
    private int chipCount;
    private String name;
    private int seatNumber;
    private Bitmap avatar;
    // TODO add sound as a feature

    //region Constructors
    public Player(String name, int chipCount, int seatNumber) {
        this.isOut = false;
        this.name = name;
        this.chipCount = chipCount;
        this.seatNumber = seatNumber;
    }

    public Player(Parcel p) {
        Object[] array = p.readArray(Object.class.getClassLoader());

        isOut = (Boolean) array[0];
        chipCount = (Integer) array[1];
        seatNumber = (Integer) array[2];
        name = (String) array[3];
        avatar = (Bitmap) array[4];
    }
    //endregion

    //region Getters
    public int getChipCount(){
        return chipCount;
    }
    public String getName(){
        return name;
    }
    public int getSeatNumber(){
        return seatNumber;
    }
    //endregion

    // region Setters
    public void setChipCount(int count){
        this.chipCount = count;
    }
    public void setName(String name){
        this.name = name;
    }
    // Is this needed?
    public void setSeatNumber(int seatNumber){
        this.seatNumber = seatNumber;
    }

    public void setAvatar(Bitmap avatar) {
        this.avatar = avatar;
    }

    public Bitmap getAvatar() {
        return avatar;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        Object[] array = new Object[5];
        array[0] = isOut;
        array[1] = chipCount;
        array[2] = seatNumber;
        array[3] = name;
        array[4] = avatar;

        dest.writeArray(array);
    }

    public String toString(){
        return name;
    }
    //endregion

    public static final Creator<Player> CREATOR = new Creator<Player>() {

        @Override
        public Player createFromParcel(Parcel source) {
            return new Player(source);
        }

        @Override
        public Player[] newArray(int size) {
            return new Player[size];
        }
    };
}
