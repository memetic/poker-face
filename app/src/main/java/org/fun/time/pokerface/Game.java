package org.fun.time.pokerface;

/**
 * Created by Julieeeee on 10/21/2014.
 */
public class Game {

    public static final int MAX_TABLE_SIZE = 12;
    private Player[] currentPlayers;
    private int dealer;
    private long currentBlind;
    private int maxNumberOfRaises;
    private Hand currentHand;

    // for debugging (total amount of money in game)
    private long stakes;

    public Game(Player[] players, int dealer, int currentBlind, int maxNumberOfRaises) {
        this.currentPlayers = players;
        this.dealer = dealer;
        this.currentBlind = currentBlind;
        this.maxNumberOfRaises = maxNumberOfRaises;

        initNewHand();

        this.stakes = 0;
        for(Player player : players)
            stakes += player.getChipCount();
    }

    private void initNewHand() {
        currentHand = new Hand();
    }

    public Player[] getPlayers() {
        return currentPlayers;
    }

    public void RemovePlayer(int playerNum){
        this.currentPlayers[playerNum] = null;
    }
}
