package org.fun.time.pokerface;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by Julieeeee on 11/7/2014.
 */
public class EditPlayerPage extends Activity {

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_player_page);
        Intent intent = getIntent();

        // TODO catch error if position is -1

        String name = intent.getStringExtra(AddPlayerPage.NAME_EXTRA);

        TextView nameView = (TextView)this.findViewById(R.id.player_name);
        nameView.setText(name);
    }
}
