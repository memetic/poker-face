package org.fun.time.pokerface;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class FrontPage extends Activity {

   private static final String START_NEW_GAME = "org.fun.time.pokerface.FrontPage.START_NEW_GAME";

   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_front_page);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.front_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent openHelpPage = new Intent(this, HelpPage.class);
            startActivity(openHelpPage);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

   public void exit(View view) {
      this.finish();
   }

   public void newGame(View view) {
      Intent i = new Intent(this, AddPlayerPage.class);
      i.putExtra(START_NEW_GAME, Boolean.TRUE);
      startActivity(i);
   }
}
