package org.fun.time.pokerface;

/**
 * Created by Admin on 10/30/2014.
 */
public class Round {

    public void setFirstToAct(int firstToAct) {
        this.firstToAct = firstToAct;
    }

    public static enum Name {
        PREFLOP,
        FLOP,
        TURN,
        RIVER
    }

    private int firstToAct;
    private int numOfRaises;
    private long[] currentBets;
    private final Name roundName;

    public Round(int firstToAct, Name round) {
        this.firstToAct = firstToAct;
        this.roundName = round;
        this.numOfRaises = 0;
        this.currentBets = new long[Game.MAX_TABLE_SIZE];
    }

    public String getName() {
        return roundName.name();
    }
}
