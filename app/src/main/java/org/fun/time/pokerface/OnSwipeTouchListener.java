package org.fun.time.pokerface;

import android.content.Context;
import android.graphics.Rect;
import android.os.Vibrator;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.view.View.OnTouchListener;
import android.widget.AbsListView.OnScrollListener;

/**
 * This OnSwipeTouchListener class implements the {@linkplain OnTouchListener}
 * interface to create callbacks for Swipe left and right, on touch and on long touch. This class
 * is meant to be used with a ListView which supports vertical scrolling. To integrate scrolling
 * and swiping, register this object with {@linkplain ListView#setOnTouchListener(OnTouchListener)}
 * and the OnScrollListener returned from {@linkplain #makeScrollListener()}
 * with {@linkplain ListView#setOnScrollListener(OnScrollListener)}.
 * <br/>
 * Implementations of this class can override default values for touch sensitivity and length.
 *
 * @author ActiveNerd
 * @since 11/11/2014.
 */
public abstract class OnSwipeTouchListener implements OnTouchListener {

    //region swipe modes
    private enum SwipeModes {
        UNDETERMINED,
        CLICK,
        LONG_CLICK,
        SWIPE_RIGHT,
        SWIPE_LEFT,
        VERTICAL_SCROLL,
        VERTICAL_FLING
    }
    //endregion

    private final ListView rootView;
    private final Context context;
    private View touchedView;
    private float initDownX;
    private SwipeModes touchResolution;
    private long touchTime;

    //region swipe thresholds
    private int slideBackDuration = 500;
    private int swipeLength = 600;
    private int slopSpace = 100;
    private int negativeSlopSpace = -slopSpace;
    private int longClickLength = 600;
    //endregion

    public OnSwipeTouchListener(Context ctx, ListView view) {
        context = ctx;
        rootView = view;
        touchResolution = SwipeModes.UNDETERMINED;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        // Disable all callbacks if scrolling is occurring
        if (touchResolution == SwipeModes.VERTICAL_SCROLL || touchResolution == SwipeModes.VERTICAL_FLING) {
            // If there was scrolling but the finger was lifted, allow other touch actions
            if (event.getActionMasked() == MotionEvent.ACTION_UP) {
                touchResolution = SwipeModes.UNDETERMINED;
            }
            return false;
        }

        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                int childCount = rootView.getChildCount();
                int[] location = new int[2];
                rootView.getLocationOnScreen(location);
                // find the touch location relative to the view
                int localX = (int) (event.getRawX() - location[0]);
                int localY = (int) (event.getRawY() - location[1]);

                // determine which view was clicked
                View child;
                Rect r = new Rect();
                for (int i = 0; i < childCount; i++) {
                    child = rootView.getChildAt(i);
                    child.getHitRect(r);

                    if (r.contains(localX, localY)) {
                        touchedView = child;
                        initDownX = event.getRawX();
                        touchTime = System.currentTimeMillis();
                        break;
                    }
                }

                return false;
            case MotionEvent.ACTION_UP:
                float absDeltaX = Math.abs(event.getRawX() - initDownX);

                if (touchResolution == SwipeModes.UNDETERMINED) {
                    touchResolution = SwipeModes.CLICK;
                    onItemClick(rootView, touchedView);
                } else if (touchResolution == SwipeModes.LONG_CLICK) {
                    // Do nothing. The Long Click action should have already taken effect
                } else if (absDeltaX >= swipeLength) {
                    touchResolution = (event.getRawX() > initDownX) ? SwipeModes.SWIPE_RIGHT : SwipeModes.SWIPE_LEFT;
                    onItemSwipe(rootView, touchedView, event.getRawX() > initDownX);
                } else {
                    // Finger was lifted during swipe but not enough to do swipe activity
                    resetViewAnimation();
                }

                touchResolution = SwipeModes.UNDETERMINED;
                return true;
            case MotionEvent.ACTION_MOVE:
                // Check the status.
                float deltaX = event.getRawX() - initDownX;

                if (touchResolution == SwipeModes.UNDETERMINED) {
                    // try to determine which one it is

                    // check to see if they have started a swipe
                    if (Math.abs(deltaX) > slopSpace) {
                        touchResolution = (deltaX > 0) ? SwipeModes.SWIPE_RIGHT : SwipeModes.SWIPE_LEFT;
                    }

                    // check to see if it's a long touch
                    if (touchTime + longClickLength < System.currentTimeMillis()) {
                        touchResolution = SwipeModes.LONG_CLICK;
                        // Set the touch time very high so it will not trigger again
                        touchTime = Long.MAX_VALUE;

                        onItemLongClick(rootView, touchedView);
                        return true;
                    }
                }

                if (touchResolution == SwipeModes.SWIPE_LEFT || touchResolution == SwipeModes.SWIPE_RIGHT) {
                    if (touchResolution == SwipeModes.SWIPE_LEFT) {
                        // check to see if it should be switched to swipe right
                        if (deltaX > slopSpace) {
                            touchResolution = SwipeModes.SWIPE_RIGHT;
                        } else {
                            // translate the view left
                            touchedView.setTranslationX(deltaX < negativeSlopSpace ? deltaX : negativeSlopSpace);
                        }
                    } else if (touchResolution == SwipeModes.SWIPE_RIGHT) {
                        // check to see if it should be switched to swipe left
                        if (deltaX + slopSpace < 0) {
                            touchResolution = SwipeModes.SWIPE_LEFT;
                        } else {
                            // translate the view right
                            touchedView.setTranslationX(deltaX > slopSpace ? deltaX : slopSpace);
                        }
                    }

                    return true;
                }

                break;
            default:
                Log.e(OnSwipeTouchListener.class.getName(), "Unhandled motion event: " + event);
        }
        return false;
    }

    /**
     * Animate the translated view back to it's original position
     */
    private void resetViewAnimation() {
        View tempView = touchedView;
        TranslateAnimation ta = new TranslateAnimation(tempView.getTranslationX(), 0, 0, 0);
        ta.setDuration(slideBackDuration);
        tempView.setTranslationX(0);
        tempView.startAnimation(ta);

        touchedView = null;
    }

    /**
     * Creates a scroll listener to work with the touch listener. This method should
     * be called the returned listener registered with the ListView using this OnTouchListener
     * @return a ScrollListener to register with the ListView
     */
    public OnScrollListener makeScrollListener() {
        return new OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (view == rootView) {
                    if (scrollState == SCROLL_STATE_TOUCH_SCROLL) {
                        touchResolution = SwipeModes.VERTICAL_SCROLL;
                    } else if (scrollState == SCROLL_STATE_FLING) {
                        touchResolution = SwipeModes.VERTICAL_FLING;
                    } else if (scrollState == SCROLL_STATE_IDLE) {
                        if (touchResolution == SwipeModes.VERTICAL_FLING) {
                            // called if there was a vertical fling and it now stopped
                            touchResolution = SwipeModes.UNDETERMINED;
                        } else {
                            // called on trackball scroll
                        }
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                // No need to do anything
            }
        };
    }

    /**
     * The callback that is called when an item is (short) clicked
     * @param parent the parent view that this touch listener is on
     * @param view the view that was clicked
     */
    public void onItemClick(AdapterView<?> parent, View view) {}

    /**
     * The callback that is called when an item is long clicked
     * @param parent the parent view that this touch listener is on
     * @param view the view that was clicked
     */
    public void onItemLongClick(AdapterView<?> parent, View view) {
        Vibrator v = (Vibrator) this.context.getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(100);
        this.onItemClick(parent, view);
    }

    /**
     * The callback that is called when an item is long clicked
     * @param parent the parent view that this touch listener is on
     * @param view the view that was clicked
     * @param swipeRight <code>true</code> if the swipe was left to right.
     * <code>false</code> if the swipe was right to left.
     */
    public void onItemSwipe(AdapterView<?> parent, View view, boolean swipeRight) {}

    /**
     * @param slideBackDuration Duration (ms) of the animation to return a ListView row
     * to it's original orientation. (Default: 500)
     */
    public void setSlideBackDuration(int slideBackDuration) {
        this.slideBackDuration = slideBackDuration;
    }

    /**
     * @param swipeLength The DeltaX returned to swipe a ListView row to trigger the
     * onItemSwipe callback. (Default: 600)
     */
    public void setSwipeLength(int swipeLength) {
        this.swipeLength = swipeLength;
    }

    /**
     * @param slopSpace The tolerance given to a touch before it is considered a swipe
     * (Default: 100)
     */
    public void setSlopSpace(int slopSpace) {
        this.slopSpace = slopSpace;
        this.negativeSlopSpace = (-1) * slopSpace;
    }

    /**
     * @param longClickLength The length (ms) of a touch before it triggers the long touch callback
     * (Default: 600)
     */
    public void setLongClickLength(int longClickLength) {
        this.longClickLength = longClickLength;
    }
}
