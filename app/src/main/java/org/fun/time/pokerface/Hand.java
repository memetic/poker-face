package org.fun.time.pokerface;

import org.fun.time.pokerface.Round.Name;

/**
 * Created by Admin on 10/30/2014.
 */
public class Hand {
    private long totalPot;
    private Round currentRound;

    public Hand () {
        this(Name.PREFLOP);
    }

    public Hand(Round.Name currentRound) {
        this.currentRound = new Round(0, currentRound);
    }

    public Hand setFirstToAct(int seat) {
        currentRound.setFirstToAct(seat);
        return this;
    }

    public String getRoundName() {
        return currentRound.getName();
    }
}
